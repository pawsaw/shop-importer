package de.commercetools.shop.importer.model;

import com.google.common.collect.ImmutableCollection;

/**
 * Represents a Shop as loaded from a given datasource or created manually using
 * an implementation of ShopBuilder. The ShopBuilder ensures that all orders belong
 * to a customer of the same shop.
 *
 * Created by Pawel Sawicki on 25.03.15.
 */
public interface Shop {

    /**
     * @return A collection of all customers.
     */
    ImmutableCollection<Customer> getAllCustomers();

    /**
     * @return A collection of all orders.
     */
    ImmutableCollection<Order> getAllOrders();
}
