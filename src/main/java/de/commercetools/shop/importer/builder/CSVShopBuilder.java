package de.commercetools.shop.importer.builder;

import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Currency;
import java.util.UUID;


/**
 * The CSVShopBuilder has an additional method for loading data (customer and orders) from CSV.
 */
public class CSVShopBuilder extends SimpleShopBuilder {

    // region PUBLIC_TYPES

    /**
     * The strategy how to act on CSV Record errors.
     */
    public enum CsvLoadingStrategy {
        SKIP_RECORDS_WITH_ERROR,
        EXCEPTION_ON_RECORDS_WITH_ERROR
    }

    // endregion PUBLIC_TYPES

    // region PUBLIC_METHODS

    /**
     *
     *
     * @param csvURL
     * @param csvLoadingStrategy
     * @return
     * @throws ShopCSVParseException
     * @throws IOException
     */
    public CSVShopBuilder addCSVContent(final URL csvURL, final CsvLoadingStrategy csvLoadingStrategy) throws ShopCSVParseException, IOException {
        final CSVParser parser = CSVParser.parse(csvURL, Charset.defaultCharset(), CSVFormat.RFC4180);

        ShopCSVRecordHolder shopCSVRecord = new ShopCSVRecordHolder();
        Customer customer = null;
        Order order = null;

        for (CSVRecord csvRecord : parser) {
            shopCSVRecord.setRecord(csvRecord);

            if (shopCSVRecord.isValid()) {

                if (shopCSVRecord.isHeader()) {
                    continue;
                }

                customer = new Customer();
                customer.setFirstName(shopCSVRecord.getFirst());
                customer.setLastName(shopCSVRecord.getLast());
                customer.setAge(shopCSVRecord.getAge());
                customer.setCity(shopCSVRecord.getCity());
                customer.setState(shopCSVRecord.getState());
                customer.setStreet(shopCSVRecord.getStreet());
                customer.setZip(shopCSVRecord.getZip());
                // generate a simple ID
                customer.setId(generateUniqueCustomerId(customer));
                addCustomer(customer);

                order = new Order();
                order.setCustomerId(customer.getId());
                order.setCentAmount(shopCSVRecord.getDollar());
                order.setCurrency(CURRENCY_DOLLAR);
                order.setPick(shopCSVRecord.getPick());
                order.setId(UUID.randomUUID().toString());
                addOrder(order);

            } else if (csvLoadingStrategy.equals(CsvLoadingStrategy.SKIP_RECORDS_WITH_ERROR)) {
                continue;
            } else {
                throw new ShopCSVParseException(String.format("Illegal CSV record: '%s'.", csvRecord.toString()));
            }
        }

        return this;
    }

    @Override
    public CSVShopBuilder addCustomer(Customer customer) {
        super.addCustomer(customer);
        return this;
    }

    @Override
    public CSVShopBuilder addOrder(Order order) {
        super.addOrder(order);
        return this;
    }

    // endregion PUBLIC_METHODS

    // region PRIVATE_METHODS

    private String generateUniqueCustomerId(final Customer customer) {

        if (customer.getId() != null) {
            return customer.getId();
        }

        String uniqueCustomerId = new StringBuilder()
                .append(customer.getFirstName())
                .append(customer.getLastName())
                .append(customer.getAge())
                .append(customer.getStreet())
                .append(customer.getState())
                .append(customer.getZip())
                .toString();

        byte[] arrayBytes = DIGEST.digest(uniqueCustomerId.getBytes());
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }

        uniqueCustomerId = stringBuffer.toString();

        return uniqueCustomerId;
    }


    // endregion PRIVATE_METHODS

    // region PRIVATE_CONSTANTS

    static final Currency CURRENCY_DOLLAR = Currency.getInstance("USD");
    static MessageDigest DIGEST;
    static {
        try {
            DIGEST = MessageDigest.getInstance("SHA");
        } catch (NoSuchAlgorithmException e) {
            // won't happen.
        }
    }

    // endregion PRIVATE_CONSTANTS

    // region PRIVATE_TYPES

    // seq,first,last,age,street,city,state,zip,dollar,pick
    private class ShopCSVRecordHolder {


        CSVRecord record;

        public void setRecord(CSVRecord record) {
            this.record = record;
        }

        public int getSeq() {
            return Integer.parseInt(record.get(0));
        }

        public String getFirst() {
            return record.get(1);
        }

        public String getLast() {
            return record.get(2);
        }

        public int getAge() {
            return Integer.parseInt(record.get(3));
        }

        public String getStreet() {
            return record.get(4);
        }

        public String getCity() {
            return record.get(5);
        }

        public String getState() {
            return record.get(6);
        }

        public String getZip() {
            return record.get(7);
        }

        public int getDollar() {
            return (int) (Double.parseDouble(record.get(8).substring(1)) * 100);
        }

        public String getPick() {
            return record.get(9);
        }

        public boolean isValid() {

            if (record.size() != 10) {
                return false;
            }

            if (record.get(0).equals("seq")) {
                return true;
            }

            // TODO: further checks...

            return true;
        }

        public boolean isHeader() {
            return record.get(0).equals("seq");
        }
    }

    // endregion PRIVATE_TYPES

    // region FIELDS

    // endregion FIELDS


}
