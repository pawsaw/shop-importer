package de.commercetools.shop.importer.builder;

/**
 * Created by Pawel Sawicki on 26.03.15.
 */
public class ShopBuilderException extends Exception {

    public ShopBuilderException(String message) {
        super(message);
    }
}
