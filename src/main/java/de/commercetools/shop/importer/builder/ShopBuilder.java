package de.commercetools.shop.importer.builder;

import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import de.commercetools.shop.importer.model.Shop;

/**
 * Builder for creating online shops with a lot of customers and orders.
 *
 * Created by Pawel Sawicki on 26.03.15.
 */
public interface ShopBuilder {

    /**
     * Adds a customer to the online shop.
     *
     * @param customer The customer (with an ID).
     * @return The instance of the builder.
     */
    ShopBuilder addCustomer(final Customer customer);

    /**
     * Adds an order to the online shop.
     *
     * @param order The order (with an ID and a customer ID).
     * @return The instance of the builder.
     */
    ShopBuilder addOrder(final Order order);

    /**
     * Creates an instance of a shop consisting of the given customer and order.
     *
     * This method ensures that each order has a related customer (and some other checks...).
     *
     * @return The newly created online shop.
     * @throws ShopBuilderException If something is not consistent.
     */
    Shop build() throws ShopBuilderException;

}
