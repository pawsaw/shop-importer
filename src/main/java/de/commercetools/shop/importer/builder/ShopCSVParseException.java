package de.commercetools.shop.importer.builder;

/**
 * Created by Pawel Sawicki on 26.03.15.
 */
public class ShopCSVParseException extends Exception {


    public ShopCSVParseException(String message) {
        super(message);
    }
}
