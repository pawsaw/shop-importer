package de.commercetools.shop.importer.builder;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import de.commercetools.shop.importer.model.Shop;


/**
 * A basic implementation of ShopBuilder.
 */
public class SimpleShopBuilder implements ShopBuilder {

    // region PUBLIC_METHODS

    @Override
    public SimpleShopBuilder addCustomer(final Customer customer) {
        customerForCustomerIdBuilder.put(customer.getId(), customer);
        return this;
    }

    @Override
    public SimpleShopBuilder addOrder(final Order order) {
        ordersBuilder.add(order);
        return this;
    }

    @Override
    public Shop build() throws ShopBuilderException {

        final ImmutableList<Order> orders = ordersBuilder.build();
        final ImmutableMap<String, Customer> customerForCustomerId = customerForCustomerIdBuilder.build();

        for (final Order currOrder : orders) {
            if (!customerForCustomerId.containsKey(currOrder.getCustomerId())) {
                throw new ShopBuilderException(String.format("There is no customer with the ID '%s' for the order ID '%s'.", currOrder.getCustomerId(), currOrder.getId()));
            }
        }

        return new ShopImpl(customerForCustomerId.values(), orders);
    }

    // endregion PUBLIC_METHODS

    // region PRIVATE_TYPES

    protected final class ShopImpl implements Shop {

        private final ImmutableCollection<Customer> customers;
        private final ImmutableCollection<Order> orders;

        public ShopImpl(final ImmutableCollection<Customer> customers, final ImmutableCollection<Order> orders) {
            this.customers = customers;
            this.orders = orders;
        }

        @Override
        public ImmutableCollection<Customer> getAllCustomers() {
            return customers;
        }

        @Override
        public ImmutableCollection<Order> getAllOrders() {
            return orders;
        }
    }

    // endregion PRIVATE_TYPES

    // region FIELDS

    private final ImmutableMap.Builder<String, Customer> customerForCustomerIdBuilder = ImmutableMap.builder();
    private final ImmutableList.Builder<Order> ordersBuilder = ImmutableList.builder();

    // endregion FIELDS

}
