package de.commercetools.shop.importer.upload;

/**
 * Created by Pawel Sawicki on 26.03.15.
 */
public class ShopUploadException extends Exception {

    public ShopUploadException(String message) {
        super(message);
    }

    public ShopUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
