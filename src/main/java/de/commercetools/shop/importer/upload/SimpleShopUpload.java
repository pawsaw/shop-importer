package de.commercetools.shop.importer.upload;

import de.commercetools.shop.importer.client.Client;
import de.commercetools.shop.importer.client.ImportResult;
import de.commercetools.shop.importer.client.ImportResults;
import de.commercetools.shop.importer.errors.ClientException;
import de.commercetools.shop.importer.errors.ServiceUnavailableException;
import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import de.commercetools.shop.importer.model.Shop;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Pawel Sawicki on 26.03.15.
 */
public class SimpleShopUpload implements ShopUpload {

    private static final int BATCH_SIZE = 200;
    private static final int MAX_NUM_RETRIES = 3;
    private static final int RETRY_SLEEP_MILLIS = 2000;

    @Override
    public void upload(final Client client, final Shop shop) throws ShopUploadException {

        uploadCustomers(client, shop);
        uploadOrders(client, shop);

    }

    private void uploadCustomers(final Client client, final Shop shop) throws ShopUploadException {

        boolean isSuccess = true;

        final ArrayList<Customer> customers = new ArrayList<>(shop.getAllCustomers());
        for (int i = 0; i < customers.size(); i += BATCH_SIZE) {
            int numRetries = 0;
            boolean done = false;
            do {
                try {
                    ImportResults customerImportResults = client.importCustomer(customers.subList(i, Math.min(i + BATCH_SIZE, customers.size() - 1)));
                    for (ImportResult customerImportResult : customerImportResults.getResults()) {
                        isSuccess &= customerImportResult.isSuccess();
                        if (!isSuccess) {
                            throw new ShopUploadException(String.format("Error while uploading the Customer with the ID '%s'. Error code: '%d'", customerImportResult.getId(), customerImportResult.getErrorCode()));
                        }
                    }
                    done = true;
                } catch (ServiceUnavailableException ex) {
                    try {
                        Thread.sleep(RETRY_SLEEP_MILLIS);
                    } catch (InterruptedException e) {
                        // DO NOTHING.
                    }
                    numRetries++;
                } catch (ClientException ex) {
                    throw new ShopUploadException(String.format("Error while uploading the Customers."), ex);
                }
            } while (!done && numRetries < MAX_NUM_RETRIES);

            if (!done) {
                throw new ShopUploadException(String.format("Error while uploading the Customers."));
            }
        }
    }

    private void uploadOrders(final Client client, final Shop shop) throws ShopUploadException {

        boolean isSuccess = true;

        final ArrayList<Order> orders = new ArrayList<>(shop.getAllOrders());
        for (int i = 0; i < orders.size(); i += BATCH_SIZE) {
            int numRetries = 0;
            boolean done = false;
            do {
                try {
                    ImportResults orderImportResults = client.importOrders(orders.subList(i, Math.min(i + BATCH_SIZE, orders.size() - 1)));
                    for (ImportResult orderImportResult : orderImportResults.getResults()) {
                        isSuccess &= orderImportResult.isSuccess();
                        if (!isSuccess) {
                            throw new ShopUploadException(String.format("Error while uploading the Order with the ID '%s'. Error code: '%d'", orderImportResult.getId(), orderImportResult.getErrorCode()));
                        }
                    }
                    done = true;
                } catch (ServiceUnavailableException ex) {
                    try {
                        Thread.sleep(RETRY_SLEEP_MILLIS);
                    } catch (InterruptedException e) {
                        // DO NOTHING.
                    }
                    numRetries++;
                } catch (ClientException ex) {
                    throw new ShopUploadException(String.format("Error while uploading the Orders."), ex);
                }
            } while (!done && numRetries < MAX_NUM_RETRIES);

            if (!done) {
                throw new ShopUploadException(String.format("Error while uploading the Orders."));
            }
        }

    }


}
