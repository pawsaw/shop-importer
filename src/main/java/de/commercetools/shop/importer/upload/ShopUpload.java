package de.commercetools.shop.importer.upload;

import de.commercetools.shop.importer.client.Client;
import de.commercetools.shop.importer.model.Shop;

/**
 * Created by Pawel Sawicki on 26.03.15.
 */
public interface ShopUpload {

    void upload(final Client client, final Shop shop) throws ShopUploadException;
}
