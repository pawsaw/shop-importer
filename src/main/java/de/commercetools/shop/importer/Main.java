package de.commercetools.shop.importer;

import de.commercetools.shop.importer.builder.CSVShopBuilder;
import de.commercetools.shop.importer.builder.ShopBuilderException;
import de.commercetools.shop.importer.builder.ShopCSVParseException;
import de.commercetools.shop.importer.client.Client;
import de.commercetools.shop.importer.client.ClientFactory;
import de.commercetools.shop.importer.model.Shop;
import de.commercetools.shop.importer.upload.ShopUpload;
import de.commercetools.shop.importer.upload.ShopUploadException;
import de.commercetools.shop.importer.upload.SimpleShopUpload;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Pawel Sawicki on 25.03.15.
 */
public class Main {

    public static void main(String[] args) {

        if (args.length != 1) {
            dieWithError("usage: shop-importer <URL>\n\nURL - The URL of the CSV source file.\n");
        }

        final CSVShopBuilder builder = new CSVShopBuilder();
        try {
            final Shop shop = new CSVShopBuilder()
                    .addCSVContent(new URL(args[0]), CSVShopBuilder.CsvLoadingStrategy.SKIP_RECORDS_WITH_ERROR)
                    .build();

            try (final Client client = ClientFactory.create("public@psawicki.de")) {

                final ShopUpload shopUpload = new SimpleShopUpload();
                shopUpload.upload(client, shop);

            } catch (IOException ex) {
                dieWithError("Error while creating the client due to: " + ex.getMessage(), ex);
            } catch (ShopUploadException ex) {
                dieWithError("Error while uploading the shop data due to: " + ex.getMessage());
            }

        } catch (ShopCSVParseException ex) {
            dieWithError("Error while parsing the CSV file due to: " + ex.getMessage(), ex);
        } catch (MalformedURLException ex) {
            dieWithError("The given URL has a malformed URL.", ex);
        } catch (IOException e) {
            dieWithError("General IO Exception due to: " + e.getMessage(), e);
        } catch (ShopBuilderException ex) {
            dieWithError("Error while building the shop due to: " + ex.getMessage(), ex);
        }
    }

    private static void dieWithError(final String message, final Exception ex) {
        System.err.println(message);
        ex.printStackTrace();
        System.exit(1);
    }

    private static void dieWithError(final String message) {
        System.err.println(message);
        System.exit(1);
    }
}
