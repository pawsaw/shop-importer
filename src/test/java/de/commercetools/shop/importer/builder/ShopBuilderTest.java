package de.commercetools.shop.importer.builder;

import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import de.commercetools.shop.importer.model.Shop;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShopBuilderTest {

    @Test
    public void testBuild() {

        Customer customer1 = new Customer();
        customer1.setId("1");

        Customer customer2 = new Customer();
        customer2.setId("2");

        Order order1 = new Order();
        order1.setId("1");
        order1.setCustomerId(customer1.getId());

        Order order2 = new Order();
        order2.setId("2");
        order2.setCustomerId(customer2.getId());

        Shop shop = null;
        try {
            shop = new SimpleShopBuilder()
                    .addCustomer(customer1)
                    .addCustomer(customer2)
                    .addOrder(order1)
                    .addOrder(order2)
                    .build();
        } catch (ShopBuilderException e) {
            fail(e.getMessage());
        }

        assertTrue("Wrong number of customers in shop.", shop.getAllCustomers().size() == 2);
        assertTrue("Wrong number of orders in shop.", shop.getAllOrders().size() == 2);


    }

    @Test(expected = ShopBuilderException.class)
    public void testBuildException() throws ShopBuilderException {
        final ShopBuilder shopBuilder = new SimpleShopBuilder();

        Customer customer1 = new Customer();
        customer1.setId("1");


        Order order1 = new Order();
        order1.setId("1");
        order1.setCustomerId(customer1.getId());

        Order order2 = new Order();
        order2.setId("2");
        order2.setCustomerId("2"); // should throw an exception on shop build

        final Shop shop = new SimpleShopBuilder()
                    .addCustomer(customer1)
                    .addOrder(order1)
                    .addOrder(order2)
                    .build();

    }
}
