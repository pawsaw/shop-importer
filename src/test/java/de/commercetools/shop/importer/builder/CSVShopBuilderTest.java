package de.commercetools.shop.importer.builder;

import de.commercetools.shop.importer.model.Customer;
import de.commercetools.shop.importer.model.Order;
import de.commercetools.shop.importer.model.Shop;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class CSVShopBuilderTest {

    @Test
    public void testBuild() {

        Customer customer1 = new Customer();
        customer1.setId("1");

        Customer customer2 = new Customer();
        customer2.setId("2");

        Order order1 = new Order();
        order1.setId("1");
        order1.setCustomerId(customer1.getId());

        Order order2 = new Order();
        order2.setId("2");
        order2.setCustomerId(customer2.getId());

        // The test.csv contains 199 records and 1 header, no errors
        URL csvTestFileURL = getClass().getClassLoader().getResource("test.csv");

        Shop shop = null;
        try {
            shop = new CSVShopBuilder()
                    .addCustomer(customer1)
                    .addCustomer(customer2)
                    .addOrder(order1)
                    .addOrder(order2)
                    .addCSVContent(csvTestFileURL, CSVShopBuilder.CsvLoadingStrategy.SKIP_RECORDS_WITH_ERROR)
                    .build();
        } catch (ShopBuilderException | ShopCSVParseException | IOException e) {
            fail(e.getMessage());
        }

        assertTrue("Wrong number of customers in shop.", shop.getAllCustomers().size() > 2);

        // 199 in csv file + 2 added manually = 201
        assertTrue("Wrong number of orders in shop.", shop.getAllOrders().size() == 201);


    }


}
